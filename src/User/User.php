<?php
namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class User extends DB{
    public $table="users";
    public $firstName="";
    public $lastName="";
    public $email="";
    public $password="";
    public $id="";

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists('first_name',$data)){
            $this->firstName=$data['first_name'];
        }
        if(array_key_exists('last_name',$data)){
            $this->lastName=$data['last_name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        return $this;


    }

    public function store(){
        $query="INSERT INTO `loginsignupb22`.`users` (`first_name`, `last_name`, `email`, `password`) VALUES ('".$this->firstName."', '".$this->lastName."', '".$this->email."', '".$this->password."')";
        $result= mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("../../index.php");
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Fail!</strong> Data has not been stored successfully.
                </div>");
            Utility::redirect("../../index.php");
        }

    }




}
